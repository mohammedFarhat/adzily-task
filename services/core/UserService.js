const BaseService = require('../BaseService');
const {
    User,
} = require("../../db/models/index");
const bcrypt = require('bcrypt')
const {ROLES_NAMES} = require('../../config/auth/roles')

class UserService extends BaseService {

    async register(data) {
        try {
            const checkEmail = await this.getUserByEmail(data.email)
            if (checkEmail)
                throw new this.ValidationError(0, `this email already exist`)
            data.roles = [ROLES_NAMES.CUSTOMER]
            const user = await this.create(data);
            return user
        } catch (e) {
            throw e
        }

    }

    async login(data) {
        try {
            const user = await this.getUserByEmail(data.email);
            // for a security reason the err msg is the same
            if (!user)
                throw new this.ValidationError(0, `email or password is not correct...please try again`, 'الايميل او الرقم السري غير صحيح');
            const checkPass = await this.checkLoginPassword(data.password, user.password)
            if (!checkPass)
                throw new this.ValidationError(0, `email or password is not correct...please try again`, 'الايميل او الرقم السري غير صحيح');

            return user;
        } catch (e) {
            throw e
        }
    }


    async create(data) {
        return await User.create(data)
    }

    async getUserByEmail(email) {
        return await User.getOne({email: email, is_archived: false})
    }

    async checkLoginPassword(givenPass, originalPass) {
        const match = await bcrypt.compare(givenPass, originalPass);
        if (!match) {
            return false
        }
        return true
    }

}

module.exports = new UserService()