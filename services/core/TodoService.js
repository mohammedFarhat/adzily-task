const BaseService = require('../BaseService');
const {
    Todo,
} = require("../../db/models/index");
const {TODO_STATUS}=require('../../config/constants/todo')
class TodoService extends BaseService {

    async createTodo(data) {
        try {
            data.status=[{
                text:TODO_STATUS.PENDING,
            }]
            return await Todo.create(data)
        } catch (e) {
            throw e
        }

    }

    async getUserTodoList(userId, params = {}, pagination = false) {
        const nParams = params;
        let paginate = pagination;
        let query = {}
        query['user'] = userId
        if (nParams.pagination) {
            paginate = this.utils.parseBoolean(nParams.pagination);
        }
        if (nParams.title)
            query['title'] = {$regex: new RegExp(nParams.title, 'i')}
        if(nParams.status)
            query['status.0.text']=nParams.status
        return await Todo.getAll(query, nParams, paginate)
    }


}

module.exports = new TodoService()