const _ = require('lodash');
const ADMIN = 'admin';
const CUSTOMER = 'customer';
const ROLE_NAMES_ENUM = [
    ADMIN,
    CUSTOMER,
];
const ROLES_NAMES = {
    ADMIN,
    CUSTOMER,
};

module.exports = {ROLE_NAMES_ENUM, ROLES_NAMES};
