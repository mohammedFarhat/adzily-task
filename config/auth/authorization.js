const AccessControl = require('accesscontrol');
const RESOURCE_NAMES = require('./resource_names');
const {ROLES_NAMES} = require('./roles');
const grantsObject = {
    [ROLES_NAMES.CUSTOMER]: {
        [RESOURCE_NAMES.TODO_LIST]: {
            'read:own': ['*'], 'create:own': ['*'], 'delete:own': ['*'], 'update:own': ['*']
        },

    },

};
module.exports = new AccessControl(grantsObject);
