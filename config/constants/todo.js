const TODO_STATUS_ENUM = ['pending','doing','done'];
module.exports = {
    TODO_STATUS_ENUM,
    TODO_STATUS:{
        PENDING:TODO_STATUS_ENUM[0],
        DOING:TODO_STATUS_ENUM[1],
        DONE:TODO_STATUS_ENUM[2],
    }
};
