const Joi = require('joi')
module.exports = {
    addTodoSchema: Joi.object().keys({
        title: Joi.string().min(1).required(),
        description: Joi.string(),

    }).options({stripUnknown: true}),

};
