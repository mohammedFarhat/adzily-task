const mongoose = require('mongoose');
const {Schema} = mongoose;
const bcrypt = require('bcrypt');
const crypto = require('../../helpers/crypto');
const {ROLE_NAMES_ENUM, ROLES_NAMES} = require('../../config/auth/roles');
const {LANGUAGES_ENUM, LANGUAGES} = require('../../config/constants/languages');

const userSchema = new Schema({
    name: {type: String, trim: true},
    email: {type: String, trim: true},
    password: {type: String},
    roles: {type: Array, default: ['customer'], required: true},
    token: {type: String, trim: true},
    is_archived: {type: Boolean, default: false},
});


userSchema.pre('save', function (next) {

    this.token = this.generateToken();

    next();
});
userSchema.pre('save', function (next) {
    let user = this;

    if (this.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, (err, hash) => {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        next();
    }
});


userSchema.methods.generateToken = function () {
    const token = crypto.generateJwtToken({
        sub: this._id,
        mobile: this.mobile,
        roles: this.roles,
    });
    return token;
};
userSchema.methods.generateSmsToken = function () {
    return crypto.generateRandomNumber();
};
userSchema.pre("update", function (next) {
    bcrypt.hash(user.password, 10, (err, hash) => {
        user.password = hash;
        user.has_password = hash;
        next();
    });
});


userSchema.index({email: 1});
module.exports = mongoose.model('User', userSchema);
