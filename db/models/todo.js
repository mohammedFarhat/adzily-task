const mongoose = require('mongoose');
const {TODO_STATUS_ENUM,TODO_STATUS}=require('../../config/constants/todo')
const {Schema} = mongoose;
const statusSchema = new Schema({
    text: {type: String, default: TODO_STATUS.PENDING, enum: TODO_STATUS_ENUM, required: true},
    created_at: {
        type: Date,
        default: Date.now()
    },
}, {_id: false});
const todoSchema = new Schema({
    title: {type: String, trim: true},
    description: {type: String, trim: true},
    user: {type: mongoose.Types.ObjectId,ref:'User'},
    status: [statusSchema],
});

todoSchema.index({user: 1});
module.exports = mongoose.model('Todo', todoSchema);
