const User = require('../ModelsFactory').create('User');
const Todo = require('../ModelsFactory').create('Todo');

module.exports = {
    User,
    Todo
};
