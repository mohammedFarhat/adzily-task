const _ = require('lodash');
const jwt = require('jsonwebtoken');

const {User} = require('../../db/models');
const config = require('../../config');
const {
    UnauthenticatedError,
    UnauthorizedError,
} = require('../../helpers/errors/index');

module.exports = async (req, res, next) => {

    let {authorization} = req.headers;
    jwt.verify(authorization, config.auth.local.key, async (err, decoded) => {
        if (err || _.isNil(decoded)) {
            return next(new UnauthenticatedError());
        }
        try {
            const obj = decoded;
            if (_.isNil(obj.roles)) {
                const message = `Token Problem`;
                return next(new UnauthorizedError(0, message));
            }

                const user = await User.getOne({_id: obj.sub, is_archived: false});

            if (_.isNil(user)) {
                const message = 'User account is not exists';
                return next(new UnauthorizedError(0, message, ' المستخدم غير موجود'));
            }
            req.user = user;
            req.user_id = String(user._id);
            next();
        } catch (e) {
            next(e);
        }

    });

};
