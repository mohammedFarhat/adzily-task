const BaseController = require("../BaseController");
const express = require("express");
const router = express.Router({mergeParams:true});
const TodoService = require("../../services/core/TodoService");
const {addTodoSchema}=require('../../validations/todo')
const RESOURCE_NAMES = require("../../config/auth/resource_names");


router.post('/', async (req, res, next) => {
    try {
        await req.authorize(req.user, RESOURCE_NAMES.TODO_LIST, ['createOwn'], async () => req.user_id === req.params.userId);
        const data = req.body;
        await req.validate(addTodoSchema, data);
        data.user=req.params.userId
        const result = await TodoService.createTodo(data);
        res.send(result);
    } catch (err) {
        next(err);
    }
});
router.get('/', async (req, res, next) => {
    try {
        await req.authorize(req.user, RESOURCE_NAMES.TODO_LIST, ['readOwn'], async () => req.user_id === req.params.userId);
        const result = await TodoService.getUserTodoList(req.user_id,req.query);
        res.send(result);
    } catch (err) {
        next(err);
    }
});

module.exports = new BaseController("/users/:userId/todoList", "private", router);
