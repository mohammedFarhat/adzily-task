const path = require("path");
const BaseController = require("../BaseController");
const express = require("express");
const router = express.Router();
const userService = require("../../services/core/UserService");
const {loginSchema, registrationSchema} = require('../../validations/userValidation')
const config = require('../../config/index')
router.get("/", async (req, res, next) => {
    return res.send(config.app);
});

router.post("/register", async (req, res, next) => {
    try {
        const data = req.body;
        await req.validate(registrationSchema, data);
        const user = await userService.register(data);
        res.send(user);
    } catch (err) {
        next(err);
    }
});
router.post("/login", async (req, res, next) => {
    try {
        const data = req.body;
        await req.validate(loginSchema, data);
        const user = await userService.login(data);
        res.send(user);
    } catch (err) {
        next(err);
    }
});
router.get("/docs", async (req, res, next) => {
    try {
        const view = path.join(__dirname, "../../docs/swagger-ui/index.html");
        return res.sendFile(view);
    } catch (err) {
        return next(err);
    }
});

module.exports = new BaseController("/", "public", router);
